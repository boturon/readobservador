#!/usr/bin/env python
# coding: utf-8

from bs4 import BeautifulSoup
import urllib3, sys, re

def getPage(url):
    http = urllib3.PoolManager()
    response = http.request('GET', url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(response.data, features="html.parser")
    return soup

def outputHTML(page):
    try:
        html = "".join([str(x) for x in page.find('div',class_= "article-body-content").find_all('p')])
        author = ' '.join(re.findall('[A-Z][^A-Z]*', page.find('div',class_="columnist-info-name").get_text().replace("\n", "").replace(" ", "")))
    except AttributeError:
        html = "".join(sum([[str(j) for j in i.find_all("p") if len(j.find_all("iframe")) != 1 and not (str(j)[:5]=="<p><a" and str(j)[-8:]=='</a></p>')] for i in page.find_all("div", class_="article-body-content")], []))
        author = ' '.join(re.findall('[A-Z][^A-Z]*', page.find("div",class_="author-name").text.replace("\n", "").replace(" ", "")))
    date = page.find('time').get_text()
    title = page.find('title').get_text()
    output = """
    <!DOCTYPE html>
    <html>
        <head>
            <h1>{}</h1>
            <h3>{}, {}, <a href="{}">URL</a></h3>
        </head>
        <body>
            {}
        </body>
    </html>
    """.format(title, author, date, sys.argv[1], html)
    with open(re.sub('[^A-Za-z0-9]+', '', '{}{}{}'.format(author, date, title))+'.html', "w") as file: file.write(output)

outputHTML(getPage(sys.argv[1]))