# readObservador

This code allows you to read the text that is freely provided by Observador.

## Instructions

0. (If needed) Create a conda environment

```
$ conda create -n observador -c conda-forge beautifulsoup4 urllib3
```

1. Execute

```
$ ~/miniconda3/envs/observador/bin/python ./script.py URL
```
where URL stands for the desired url address. This creates an html file in the present directory.

2. Open HTML

```
$ firefox FILENAME.html
```
where FILENAME stands for the name of the html created in the previous step.